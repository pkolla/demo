package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {

	public static Logger logger = LoggerFactory.getLogger(DemoApplication.class);

	public void init() {
		logger.info("@@@APPLICATION STARTED...");
	}

	public static void main(String[] args) {
		logger.info("@@@APPLICATION EXECUTING...");
		SpringApplication.run(DemoApplication.class, args);
	}

	@GetMapping("get")
	public String method() {
		logger.info("@@@GET API CALLED...");
		return "welcome to Spring Boot";
	}

}
